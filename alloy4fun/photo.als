// Photo sharing social network http://alloy4fun.inesctec.pt/u6EPDxzJrQSEKRwSf

sig User {
	follows : set User,
	sees : set Photo,
	posts : set Photo,
	suggested : set User
}

sig Influencer extends User {}

sig Photo {
	date : one Day
}
sig Ad extends Photo {}

sig Day {}
// Specify the following properties.
// You can check their correctness with the different commands and
// when specifying each property you can assume all the previous ones to be true.

pred inv1 {
	// Every image is posted by one user.
	all disj x, y: User | x.posts & y.posts = none
    Photo in User.posts
}
 

pred inv2 {
	// Users cannot follow themselves.
    all u: User | not u in u.follows
}


pred inv3 {
	// Users can see ads posted by everyone, 
	// but only see non ads posted by followed users.
    all u: User | u.sees in Ad + u.follows.posts
}


pred inv4 {
	// If a user posts an ad then all its posts should be labeled as ads. 
    all u: User | u.posts in Ad or (u.posts & Ad = none) 
}


pred inv5 {
	// Influencers are followed by everyone else.
    all i: Influencer | all u: User - i | i in u.follows
}


pred inv6 {
	// Influencers post every day.
    all i: Influencer | i.posts.date = Day
}


pred inv7 {
	// Suggested are other users followed by followed users, but not yet followed.
	all u: User | u.suggested = u.follows.follows - u.follows - u
}


pred inv8 {
	// A user only sees ads from followed or suggested users.
    all u: User| all s: u.sees | s in Ad => s in u.follows.posts + u.suggested.posts
}
