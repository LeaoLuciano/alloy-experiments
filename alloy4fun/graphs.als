// Graph alloy4fun.inesctec.pt/vBnLJx7x62yWHpWcN

/* 
Each node as a set of outgoing edges, representing a directed graph without multiple edged.
*/
sig Node {
	adj : set Node
}

/*
The graph is undirected, ie, edges are symmetric.
http://mathworld.wolfram.com/UndirectedGraph.html
*/
pred undirected {
    all x, y: Node | x in y.adj => y in x.adj
}

/*
The graph is oriented, ie, contains no symmetric edges.
http://mathworld.wolfram.com/OrientedGraph.html
*/
pred oriented {
    all x, y: Node | x in y.adj => not y in x.adj
}

/*
The graph is acyclic, ie, contains no directed cycles.
http://mathworld.wolfram.com/AcyclicDigraph.html
*/
pred acyclic {
    all x: Node | not x in x.^adj
}

/*
The graph is complete, ie, every node is connected to every other node.
http://mathworld.wolfram.com/CompleteDigraph.html
*/
pred complete {
    all x, y: Node | x != y iff x in y.adj
}

/*
The graph contains no loops, ie, nodes have no transitions to themselves.
http://mathworld.wolfram.com/GraphLoop.html
*/
pred noLoops {
    all x: Node | not x in x.adj 
}

/*
The graph is weakly connected, ie, it is possible to reach every node from every node ignoring edge direction.
http://mathworld.wolfram.com/WeaklyConnectedDigraph.html
*/
// pred weak[x: Node, y: Node] {
//     x in y.^adj or y in x.^adj or
//     some z: Node | z  != x and (z != y) and weak[x, z] and weak[z, y]
// }

pred weaklyConnected {
    all disj x, y: Node | y in x.^(adj + ~adj)  //y in (x.^adj + (adj.*adj.x))
}

/*
The graph is strongly connected, ie, it is possible to reach every node from every node considering edge direction.
http://mathworld.wolfram.com/StronglyConnectedDigraph.html
*/
pred stonglyConnected {
    all disj x, y: Node | x in y.^adj and y in x.^adj
}

/*
The graph is transitive, ie, if two nodes are connected through a third node, they also are connected directly.
http://mathworld.wolfram.com/TransitiveDigraph.html
*/
pred transitive {
    all x, y, z: Node | z in x.adj and y in z.adj => y in x.adj
}
