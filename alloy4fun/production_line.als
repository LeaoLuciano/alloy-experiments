// Production Line http://alloy4fun.inesctec.pt/n5rgKi6xhjymixNGy

sig Workstation {
	workers : set Worker,
	succ : set Workstation
}
one sig begin, end in Workstation {}

sig Worker {}
sig Human, Robot extends Worker {}

abstract sig Product {
	parts : set Product	
}

sig Material extends Product {}

sig Component extends Product {
	workstation : set Workstation
}

sig Dangerous in Product {}
// Specify the following properties.
// You can check their correctness with the different commands and
// when specifying each property you can assume all the previous ones to be true.

pred inv1 {
	// Workers are either human or robots
  	all w: Worker | w in Human + Robot
	
}


pred inv2 {
	// Every workstation has workers and every worker works in one workstation
    all s: Workstation | some s.workers
    Workstation.workers = Worker
    all disj x, y: Workstation | x.workers & y.workers = none 
}


pred inv3 {
	// Every component is assembled in one workstation
    all c: Component | one c.workstation
}


pred inv4 {
	// Components must have parts and materials have no parts
    all c: Component | some c.parts
    all m: Material | not some m.parts
}


pred inv5 {
	// Humans and robots cannot work together
    all s: Workstation | s.workers in Human or s.workers in Robot
}


pred inv6 {
	// Components cannot be their own parts
    all c: Component | not c in c.^parts
}


pred inv7 {
	// Components built of dangerous parts are also dangerous
    all c: Component | (some d: Dangerous | d in c.parts) => c in Dangerous
}


pred inv8 {
	// Dangerous components cannot be assembled by humans
    workers.Human & Dangerous.workstation = none
}


pred inv9 {
	// The workstations form a single line between begin and end
    all s: Workstation | lone s.succ
    all s: Workstation | s in begin.*succ
    end.succ = none
}


pred inv10 {
	// The parts of a component must be assembled before it in the production line
    all c: Component | (c.parts.workstation & c.workstation.*succ) = none
}
