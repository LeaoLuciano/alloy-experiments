// Table Reservation http://alloy4fun.inesctec.pt/sKLTRjXh6Ad9o3XQn
sig Client {}
abstract sig Table {}
one sig One,Two,Three extends Table {}

// Specify the following properties assuming the following events can happen.

// reserve[c,t] iff client c reserves table t
// use[c,t] iff client c uses table t
// stutter iff nothing happens



pred prop1 {
	// Clients can use at most one table at a time
    always all c: Client | lone t: Table | use[c, t]
}


pred prop2 {
	// Clients can only use previously reserved tables
    always all c: Client, t: Table | use[c, t] => before once reserve[c, t]

}


pred prop3 {
	// There should always be at least one unused table
    always some t: Table | not some c: Client | use[c, t]
}


pred prop4 {
	// Clients must use all the tables they reserve
    always all c: Client | all t: Table | reserve[c, t] => after eventually use[c, t]
}


pred prop5 {
	// Table two can only be used after table one
    always ((some c: Client | use[c, Two]) => before once (some c2: Client | use[c2, One]))
}

pred use_after[t1: Table, t2: Table] {
    always ((some c: Client | use[c, t1]) => before once (some c2: Client | use[c2, t2]))

}

pred prop6 {
	// Table three can only be used after tables one and two have been used
    always use_after[Three, One]
    always use_after[Three, Two]
}


// Não passou, contra-exemplo parece errado
pred prop7 {
	// After using a table a client can only use it again after reserving it
    always (all c: Client ,t: Table | use[c, t] => 
        (not (once before use[c, t])) or
        (once before reserve[c, t])
    )


}


pred prop8 {
	// No one can use the same table for ever
    always all c: Client, t: Table | use[c, t] => eventually not use[c, t]
}


pred prop9 {
	// Tables can only be used once
    always all t: Table | ((some c: Client | use[c, t]) => after (always not (some c: Client | use[c, t])))

}


pred prop10 {
	// Once in a while all the tables are free
    always eventually all c: Client, t: Table | not use[c, t]
}

pred prop11 {
	// If clients stop making reservations then tables will also stop being used
    always (
        (always not some c: Client, t: Table | reserve[c, t]) =>
        (eventually always not some c: Client, t: Table | use[c, t])
    )
}


// Não passou, contra-exemplo parece errado
pred prop12 {
	// Clients cannot use the same table twice in a row
    always not (some t: Table | (some c: Client | use[c, t]) and (some c: Client | before use[c, t]))
}
