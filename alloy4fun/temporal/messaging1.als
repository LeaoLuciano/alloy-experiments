// Messaging System 
abstract sig Node {}
one sig A,B,C extends Node {}
abstract sig Message {}
one sig Hello,World extends Message {}

// Specify the following properties assuming the following events can happen
// and that each node can only send or receive at most one message at a time.

// send[n, m] iff node n broadcasts message m
// receive[n,m] iff node n receives message m
// stutter iff nothing happens



pred prop1 {
	// Nothing will ever happen
	always stutter
}


pred prop2 {
	// Every node will send something
    all n: Node | eventually some m: Message | send[n, m]
}


pred prop3 {
	// Node A only sends Hello messages
    always all m: Message | send[A, m] => m in Hello
}


pred prop4 {
	// Any received message must have been sent before
    always all n: Node, m: Message | receive[n, m] => before once some x: Node | send[x, m]
}


pred prop5 {
	// All the nodes keep sending messages
    always all n: Node | some m: Message | eventually send[n, m]
}


pred prop6 {
	// Nodes will eventually stop sending messages
    always all n: Node | eventually always not some m: Message | send[n, m]
}


pred prop7 {
	// Nodes can only send a World after sending an Hello
    always all n: Node | send[n, World] => once send[n, Hello]
}


pred prop8 {
	// Nodes can only send a World after receiving an Hello
    always all n: Node | send[n, World] => once receive[n, Hello]

}


pred prop9 {
	// The first event that can occur is sending an Hello

    (always stutter) or 
    (eventually once ((historically stutter); (some n: Node | send[n, Hello])) ) or
    (some n: Node | send[n, Hello])
}


pred prop10 {
	// Every sent message must be later received by all other nodes
    always (all n: Node | all m: Message | send[n, m]
        => all x: Node-n | after eventually receive[x, m]
    )
}

pred prop11 {
	// Nodes cannot send repeated messages
    always (all n: Node | all m: Message | send[n, m] => not before once send[n, m])
}


fun sending: Node -> Message {
    { n: Node, m: Message | send[n, m]}
}
fun receiving: Node -> Message {
    { n: Node, m: Message | receive[n, m] }
}

// pred send[n: Node, m: Message] {
    
// }

// pred receive[n: Node, m: Message] {
    
// }

// pred stutter {
    
// }

pred prop12 {
	// After a message is sent no other message can be sent until the former is received by some node

    // always (all n: Node | some m: Message | send[n, m] 
    //     => all x: Node | all y: Message | 
    //         historically send[x, y] => some z: Node | historically receive[z, y] 
    // )
    // always (
    //     all n1, n2: Node | all disj m1, m2: Message | 
    //         (
    //             send[n1, m1] and (once send[n2, m2]) =>
    //             once some n3: Node |receive[n3, m2]
    //         ) and
    //         (eventually some n3: Node | receive[n3, m1])
    // )
    
    // always all n1: Node, m1: Message |  
    //     (send[n1, m1]) =>
    //     (all m2: Message | all n2: Node | all n3: Node | 
    //         (once send[n2, m2]) => (once receive[n3, m2]))

    // always all n1: Node, m: Message | receive[n1, m] => once some n2:Node | send[n2, m]


    always (all n1: Node, m1: Message | send[n1, m1] => 
        // after (not some n2: Node, m2: Message | send[n2, m2])
        // until some n2: Node | before once receive[n2, m1]

        (
            //(after eventually some n2: Node | receive[n2, m1]) or 
            after always (not some n2: Node, m2: Message | send[n2, m2])
        )
        or (
            not (after eventually  some n2: Node | receive[n2, m1]) releases
            after historically (not some n2: Node, m2: Message | send[n2, m2])
            
        )
        or (
            all n2: Node, m2: Message | 
                before once send[n2, m2] =>
                some n3: Node | before before once receive[n3, m2]      
        )


        


    )


    // always (all n1: Node, m1: Message | send[n1, m1] => 
    // (
    //     after (
    //         (not all n2: Node, m2: Message | send[n2, m2]) until 
    //         some n2: Node | before once receive[n2, m1]
    //     )
    // )

    // )

    // always (all n1: Node, m1: Message | send[n1, m1] => 
    //     not some n2: Node | historically receive[n2, m1]
    // )

    // always lone n1: Node, m1: Message | send[n1, m1]

    // always (
    //     // (lone n1: Node, m1: Message | send[n1, m1] )
    //     // and 
    //     (
    //     all n1: Node, m1: Message | send[n1, m1] => 
    //         all n2: Node, m2: Message | 
    //             (before once send[n2, m2]) => before once some n3: Node | receive[n3, m2] )
    //     // and
    //     // ( all n1: Node, m1: Message | some n2: Node | 
    //     //     send[n1, m1] => (after eventually receive[n2, m1]
    //     // ))
    //     // and
    //     // ( all n1: Node, m1: Message | all n2: Node | 
    //     //     send[n1, m1] => (not before once receive[n2, m1]
    //     // ))
    //     and 
    //     (Node.(sending) & Node.(receiving) = none)
    // )
}

run { always prop12 }


pred prop13 {
	// At most one send or one receive can happen at each time
    always ( lone sending + receiving and not some (sending & receiving) )
}


pred prop14 {
	// The network can stutter for at most 2 steps
    always (not (stutter; stutter; stutter))
}


pred prop15 {
	// All the receives happen after all the sends
    always (some sending => historically not some receiving)

    // always all n1, n2: Node, m1, m2: Message | send[n1, m1] triggered receive[n2, m2] 
}