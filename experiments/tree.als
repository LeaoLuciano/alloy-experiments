sig Node {
    child: set Node
}

sig Root extends Node {
    
}

fact {
    one Root
    all y : Node - Root | not Root in y.*child 
    all y : Node - Root | y in Root.*child
}

fact {
    // all disj x, y : Node | (y in x.*child) iff not (x in y.*child)
    all disj x, z : Node | 
        (z in x.child) 
        implies not (z in x.child.^child)
    all n : Node | not (n in n.child)
    some disj x, y, z: Node | z in x.child and y in x.child
    
    all x : Node - Root | one y: Node | x in y.child 
    // some child
    // one x : Node | 
    // all disj x : Node |  y : Node 
}
run test {} for exactly 2 Node