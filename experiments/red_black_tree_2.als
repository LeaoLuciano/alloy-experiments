abstract sig Node {
    left:  lone Node,
    right: lone Node,
    children: set Node,
    level: one Level
}

sig Red extends Node {

}

sig Black extends Node {

}

sig Nil extends Black {

}

sig Root in Node {

}

fact red {
    // All red children is black
    all r: Red | r.children in Black
}

fact nil {
    Nil.left = none
    Nil.right = none

    Nil.level = Last
}

fact children {
    // Children definition
    all n: Node | n.children = n.left + n.right
}

fact node {

    // all n: Node | some n.left and some n.right iff n.left != n.right

    // A node can't be its children
    all n: Node | not n in n.children
    // A tree can't have loops
    all n: Node | not n in n.^children

    // Every non nil node has some child
    all n : Node - Nil | some n.children



    // The tree only has one connected component
    // all disj x, y : Node | x in y.^children or y in x.^children

    // A node can't have more tha one parent
    all disj x, y: Node | x.children & y.children = none
    // A node can't have the same parent twice
    all x: Node | x.left & x.right = none

    // all x: Node | some y in Node - x |

    // Every node has two children
    all x: Node - Nil | some x.left and some x.right 
}

fact node_level {
    // Root is on first level
    Root.level = First

    // Children are on the next level
    // all disj x, y : Node | y in x.children => (y.level  in x.level.next)

    // Counting black nodes
    all disj x, y : Node | y in x.children => 
        (y in Black => y.level = x.level.next
        else y.level = x.level )
    
    // Every nil node is on the same level
    all disj x, y : Nil |  x.level = y.level

    
}

fact root {
    (Node = none) or one Root
    all y : Node - Root | not Root in y.^left and not Root in y.^right

    // Every node is reachable by the root
    all y : Node - Root | y in Root.^children

}

fact black {

}


sig Level {
    next: lone Level
}

sig First in Level {
}

sig Last in Level {

}


fact level {
    one First
    one Last

    all x: Level - First| x in First.^next
    all x: Level| not x in x.next

    Last.next = none

}


assert red_child {
    // If a node has exactly one child, then is a red child
    all n: Node - Nil | one (n.children & Nil) => one (n.children & Red)
}

check red_child

assert balanced {
    
}

pred add {
    
}

run {

} for 10 but 20 Node

// run {} for exactly 10 Level, 3 Node