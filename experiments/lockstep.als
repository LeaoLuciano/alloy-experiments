open util/ordering[Step]

sig Node {
    node_id: disj one Id,
    state: Step lone -> Command,
    current_step: one Step
}

sig Command {
    step: one Step
}

// TODO: 
//   use int?
//   add last
sig Step {

}

sig Id {

}

let unchanged[x] {
     ((x) = (x)') 
}

pred broadcast[n: Node, c: Command] {
    all n: Node | unchanged[n.state]
    all n: Node | unchanged[n.step]
}

pred receive[n: Node, c: Command] {
    all n: Node | unchanged[n.state]
    all n: Node | unchanged[n.step]
}

pred apply_command[n: Node, c: Command] {
    n.state' = n.state + (c.step -> c) 

    all n: Node | unchanged[n.step]
}

pred advance_step[n: Node] {
    n.step' = n.current_step.next

    all n: Node | unchanged[n.state]
    all m: Node-n | unchanged[m.current_step]
}

pred stutter {
    all n: Node | unchanged[n.state]
    all n: Node | unchanged[n.step]
}

// Global frame conditions 
fact {
    always {
        unchanged[Node]
        unchanged[Command]
        all n: Node | unchanged[n.node_id]
        all c: Command | unchanged[c.step]
    }
}

fact {
    always{
        // All broadcats eventually will be received by every node
        all n: Node, c: Command | broadcast[n, c] =>
            eventually all m: Node | receive[m, c]


        // Cannot broadcast repeated commands

        // A command cannot be sent, received, applied twice

        // A Node only sends commands of the current step



    }

}

run {}


check {
    always {
        // Check if all nodes have the same state every step
        all n: Node | 


        // Check if no command is late applied
        


    }

    // Check if all sent commands was received and applied after the last step


}