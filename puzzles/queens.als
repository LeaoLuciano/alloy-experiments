open util/integer

sig Queen {
    x: one Int,
    y: one Int
}

pred attacking[q1: Queen, q2: Queen] {
    q1.x = q2.x 
    or q1.y = q2.y
    or norm[q1.x - q2.x, q1.y - q1.y] = 0
}

let queens = {q: Queen | 1 <= q.x and q.x <= 8 and 1 <= q.y and q.y <= 8}

fact{
    all disj q1, q2: queens | not attacking[q1, q2]
}


run {

} for exactly 8 Queen

fun norm[x: Int, y: Int]: Int {
    mul[x,x] + mul[y, y]
}